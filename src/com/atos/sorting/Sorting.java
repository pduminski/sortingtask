package com.atos.sorting;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Sorting {

    private int[] table;
    private int change = 1;
    private int helper = 0;
    private int numberOfMoves = 0;

    // Constructor
    public Sorting(int[] tab) {
        table = tab;
    }
    // sort using ArrayList
    public void sortTableByArray() {
        Arrays.sort(table);
    }

    // Sort our table
    public void sortTable() {
        while(change > 0) {
            change = 0;
            for (int i = 0; i < table.length-1; i++) {
                if (table[i] > table[i + 1]) {
                    helper = table[i + 1];
                    table[i + 1] = table[i];
                    table[i] = helper;
                    change++;
                    numberOfMoves++;
                }
            }
        }
    }



    // Show the results
    public void showTable() {
        System.out.print("List: ");
        for (int i=0; i < table.length; i++) {
            System.out.print(table[i] + " ");
        }
        System.out.println(" ");
    }

    public void showTableWithoutHighestAndLowestValue() {
        System.out.print("Table without the lowest and highest number: ");
        for (int i = 1; i < table.length-1; i++) {
            System.out.print(table[i] + " ");
        }
        System.out.println(" ");
    }
    public void showNumberOfMoves() {
        System.out.println("Number of total moves needed: " + numberOfMoves);
    }
}
